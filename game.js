'use strict';

class Vector {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }
    plus(vector) {
        if (!(vector instanceof Vector)) {
            throw new Error("Можно прибавлять к вектору только вектор типа Vector");
        }
        return new Vector(this.x + vector.x, this.y + vector.y);
    }
    times(multiplier = 1) {
        return new Vector(this.x * multiplier, this.y * multiplier)
    }
}

class Actor {
    constructor(pos = new Vector(), size = new Vector(1,1), speed = new Vector()) {
        if (!(pos instanceof Vector) || !(size instanceof Vector) || !(speed instanceof Vector)) {
            throw new Error("Функция принимает только аргументы типа Vector");
        }
        this.pos = pos;
        this.size = size;
        this.speed = speed;
    }
    get top() {
        return this.pos.y;
    }
    get bottom() {
        return this.pos.y + this.size.y;
    }
    get left() {
        return this.pos.x;
    }
    get right() {
        return this.pos.x + this.size.x;
    }
    get type() {
        return 'actor';
    }
    isIntersect(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error("Функция принимает только аргумент типа Actor");
        }

        if (actor === this) {
            return false;
        }

        return this.top < actor.bottom && this.right > actor.left && this.bottom > actor.top && this.left < actor.right;
    }
    act() {}
}

class Level {
    constructor(grid = [], actors = []) {
        this.grid = grid.slice();
        this.actors = actors.slice();
        this.status = null;
        this.finishDelay = 1;
        this.height = grid.length;
        this.width = Math.max(0,...grid.map(item => item.length));
        this.player = this.actors.find(actor => actor.type === 'player');
    }
    isFinished() {
        return this.status !== null && this.finishDelay < 0;
    }
    actorAt(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error("Функция принимает только аргумент типа Actor");
        }
        return this.actors.find(levelActor => {
            return actor.isIntersect(levelActor);
        });
    }
    obstacleAt(positionTo, size) {
        if (!(positionTo instanceof Vector) || !(size instanceof Vector)) {
            throw new Error("Функция принимает только аргументы типа Vector");
        }
        const object = new Actor(positionTo, size);

        if (object.right > this.width || object.left < 0 || object.top < 0) {
            return 'wall';
        }
        if (object.bottom > this.height) {
            return 'lava';
        }
        for (let y = Math.floor(object.top); y < Math.ceil(object.bottom); y++) {
            for (let x = Math.floor(object.left); x < Math.ceil(object.right); x++) {
                if (this.grid[y][x]) {
                    return this.grid[y][x];
                }
            }
        }
    }
    removeActor(actor) {
        const indexOf = this.actors.indexOf(actor);
        if (indexOf !== -1) {
            this.actors.splice(indexOf, 1);
        }
    }
    noMoreActors(actorType) {
        return !this.actors.some(actor => actor.type === actorType);
    }
    playerTouched(objectType, object) {
        if (this.status !== null) {
            return;
        }

        if (objectType === 'lava' || objectType === 'fireball') {
            this.status = 'lost';
        }

        if (objectType === 'coin' && object) {
            this.removeActor(object);
            if (this.noMoreActors(objectType)) {
                this.status = 'won';
            }
        }
    }
}

class LevelParser {
    constructor(glossary = {}) {
        this.glossary = Object.assign({}, glossary);
    }
    actorFromSymbol(symbol) {
        if (!this.glossary || typeof this.glossary[symbol] !== 'function') {
            return;
        }
        return this.glossary[symbol];
    }
    obstacleFromSymbol(symbol) {
        if (symbol === 'x') {
            return 'wall';
        }
        if (symbol === '!') {
            return 'lava';
        }
    }
    createGrid(plan) {
        return plan.reduce((result, string) => {
            const row = [];
            for (let symbol of string) {
                row.push(this.obstacleFromSymbol(symbol));
            }
            result.push(row);
            return result;
        }, []);

    }
    createActors(actorsArr) {
        return actorsArr.reduce((result, string, y) => {
            const stringArr = string.split('');

            stringArr.forEach((symbol, x) => {
                const actorConstr = this.actorFromSymbol(symbol);
                if (actorConstr) {
                    const actor = new actorConstr(new Vector(x,y));

                    if (actor instanceof Actor) {
                        result.push(actor);
                    }
                }
            });
            return result;
        }, []);
    }
    parse(plan) {
        return new Level(this.createGrid(plan), this.createActors(plan));
    }
}

class Fireball extends Actor {
    constructor(pos = new Vector(), speed = new Vector()) {
        const size = new Vector(1,1);
        super(pos, size, speed);
    }
    get type() {
        return 'fireball';
    }
    getNextPosition(time = 1) {
        return this.speed.times(time).plus(this.pos);
    }
    handleObstacle() {
        this.speed = this.speed.times(-1);
    }
    act(time, level) {
        const nextPos = this.getNextPosition(time);
        const hasObstacle = level.obstacleAt(nextPos, this.size);
        if (hasObstacle) {
            this.handleObstacle();
        } else {
            this.pos = nextPos;
        }
    }
}

class HorizontalFireball extends Fireball {
    constructor(position = new Vector()) {
        const speed = new Vector(2,0);
        super(position, speed);
    }
}

class VerticalFireball extends Fireball {
    constructor(position = new Vector(0,0)) {
        const speed = new Vector(0,2);
        super(position, speed);
    }
}

class FireRain extends Fireball {
    constructor(position = new Vector()) {
        const speed = new Vector(0,3);
        super(position, speed);
        this.initPos = position;
    }
    handleObstacle() {
        this.pos = this.initPos;
    }
}

class Coin extends Actor {
    constructor(position = new Vector(0,0)) {
        const pos = position.plus(new Vector(0.2, 0.1));
        const size = new Vector(0.6, 0.6);
        super(pos, size);
        this.springSpeed = 8;
        this.springDist = 0.07;
        this.spring = Math.random() * Math.PI * 2;
        this.initPos = pos;
    }
    get type() {
        return 'coin';
    }
    updateSpring(time = 1) {
        this.spring = this.spring + this.springSpeed * time;
    }
    getSpringVector() {
        const y = Math.sin(this.spring) * this.springDist;
        return new Vector(0, y);
    }
    getNextPosition(time = 1) {
        this.updateSpring(time);
        const springVector = this.getSpringVector();
        return this.initPos.plus(springVector);
    }
    act(time) {
        this.pos = this.getNextPosition(time);
    }
}

class Player extends Actor {
    constructor(position = new Vector(0,0)) {
        const pos = position.plus(new Vector(0,-0.5));
        const size = new Vector(0.8, 1.5);
        const speed = new Vector();
        super(pos, size, speed);
    }
    get type() {
        return 'player';
    }
}

const actorDict = {
    '@': Player,
    'v': FireRain,
    'o': Coin,
    '=': HorizontalFireball,
    '|': VerticalFireball
};
const parser = new LevelParser(actorDict);

loadLevels().then(schemas => {
    let levels = JSON.parse(schemas);
    runGame(levels, parser, DOMDisplay)
        .then(() => alert('Вы выиграли приз!'));
});